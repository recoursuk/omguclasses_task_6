import java.util.List;

public class CollectionsDemo {
    public static int countOfEntries(List<String> coll, char c){
        int counter = 0;
        for(String elem:coll){
            if(elem == null){
                continue;
            }
            if(elem.length() > 0 && elem.charAt(0) == c){
                counter++;
            }
        }
        return counter;
    }
}
