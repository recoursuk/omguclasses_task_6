import java.util.*;
import java.util.stream.Collectors;

public class ListDemo {
    public static List<Human> getListOfSimilarName(List<Human> humanList, Human human){
        ArrayList<Human> ListOfNames = new ArrayList<>();
        for(Human elem:humanList){
            if(elem.getMiddlename().equals(human.getMiddlename())){
                ListOfNames.add(elem);
            }
        }
        return ListOfNames;
    }

    public static List<Human> getCopyWithoutHuman(List<Human> humanList, Human human){
        ArrayList<Human> res = new ArrayList<>();
        for (Human elem:humanList){
            if(!elem.equals(human)){
                res.add(elem);
            }
        }
        return res;
    }

    public static List<Set<Integer>> getIntersection(List<Set<Integer>> listSet, Set<Integer> set){
        List<Set<Integer>> res = new ArrayList<>();
        boolean flag = true;
        for(Set<Integer> elem:listSet){
            flag = true;
            for (Integer elem1: elem){
                if(set.contains(elem1)){
                    flag = false;
                    break;
                }
            }
            if(flag){
                res.add(elem);
            }
        }
        return res;
    }

    public static Set<Human> getSetWithMaxAge(List<? extends Human> humanList){
        Set<Human> res = new HashSet<>();
        int max = 0;
        for (Human elem: humanList){
            if (elem.getAge() > max){
                max = elem.getAge();
            }
        }
        for (Human elem: humanList) {
            if (elem.getAge() == max) {
                res.add(elem);
            }
        }
        return res;
    }

    public static Set<Human> getSetById(Map<Integer, Human> humanMap, Set<Integer> set){
        Set<Human> res = new HashSet<>();
        for(Map.Entry<Integer, Human> elem: humanMap.entrySet()){
            if(set.contains(elem.getKey())){
                res.add(elem.getValue());
            }
        }
        return res;
    }

    public static List<Integer> getIdListByAge(Map<Integer, Human> humanMap){
        List<Integer> res = new ArrayList<>();
        for(Map.Entry<Integer, Human> elem:humanMap.entrySet()){
            if(elem.getValue().getAge() >= 18){
                res.add(elem.getKey());
            }
        }
        return res;
    }

    public static Map<Integer, Integer> getMapByAge(Map<Integer, Human> humanMap){
        Map<Integer,Integer> res = new HashMap<>();
        for(Map.Entry<Integer, Human> elem: humanMap.entrySet()){
            res.put(elem.getKey(), elem.getValue().getAge());
        }
        return res;
    }

    public static Map<Integer, List<Human>> getHumanListByAge(Set<Human> humanSet){
        Map<Integer, List<Human>> res = new HashMap<>();
        for (Human elem:humanSet){
            if(!res.containsKey(elem.getAge())){
                res.put(elem.getAge(), new ArrayList<Human>());
            }
            res.get(elem.getAge()).add(elem);
        }
        return res;
    }

    public static Map<Integer,List<Human>> getHumanSameAgeMap(Set<Human> s){
        Map<Integer,List<Human>> nm = new HashMap<>();
        for (Human h:s) {
            int age = h.getAge();
            nm.put(age,s.stream().filter(el -> el.getAge() == age).collect(Collectors.toList()));
        }
        return nm;
    }


    public static Map<Integer, Map<Character, List<Human>>> getMapByAgeAndChar (Set<Human> set){
        Map<Integer,Map<Character,List<Human>>> nm = new HashMap<>();
        Map<Integer,List<Human>> hm = getHumanSameAgeMap(set);
        hm.forEach((key,value) -> {
            //тут должна быть гарантия того что нигде нулл не проскочит
            Map<Character, List<Human>> cm = new HashMap<>();
            for(Human h : set) {
                char firstLetter = h.getMiddlename().charAt(0);
                List<Human> l = value.
                        stream().
                        filter(el -> el.getMiddlename().charAt(0) == firstLetter).
                        sorted(Comparator.comparing(o -> (o.getMiddlename() + o.getName() + o.getLastname()))).
                        collect(Collectors.toList());
                cm.putIfAbsent(firstLetter, l);
            }
            nm.put(key,cm);
        });
        return nm;
    }

}