import java.util.*;

public class PhoneBook {
    private Map<Human, List<String>> map = new HashMap<>();

    public List<String> getNumberList(Human h){
        return map.get(h);
    }

    public void add(Human h,String s){
        map.put(h,new ArrayList<String>(Arrays.asList(s)));
    }

    public void add(Human h,List<String> l){
        map.put(h,l);
    }

    public List<String> addIfAbsent(Human h, List<String> l){
        return map.putIfAbsent(h,l);
    }

    public String addNumberIfAbsent(Human h,String s){
        if(!map.get(h).contains(s)){
            map.get(h).add(s);
        }
        return null;
    }

    public void addNumber(Human h,String s) throws NullPointerException{
        if(map.get(h) == null) {
            this.add(h, s);
        } else {
            map.get(h).add(s);
        }
    }

    public boolean removeNumber(Human h,String s){
        if(map.get(h) == null){
            throw new IllegalArgumentException("No such person");
        }
        return map.get(h).remove(s);
    }

    public List<String> removeList(Human h){
        return map.remove(h);
    }

    public Human find(String n){
        return map.entrySet().stream().filter(el -> el.getValue().contains(n)).findFirst().get().getKey();
    }

    public Map<Human,List<String>> getMapStartWith(String startString){
        Map<Human, List<String>> nm = new HashMap<>();
        map.forEach((key,value)->{
            if(key.getMiddlename().startsWith(startString)){
                nm.put(key,value);
            }
        });
        return nm;
    }

    public Map<Human, List<String>> getPhoneBook() {
        return map;
    }
}
