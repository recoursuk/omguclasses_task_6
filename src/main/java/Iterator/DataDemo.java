package Iterator;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class DataDemo {
    public static List<Integer> getAll(Data d){
        List<Integer> l = new LinkedList<>();
        for (Group group : d) {
            int[] arr = group.getData();
            for (int elem : arr) {
                if (!l.contains(elem)) {
                    boolean containInEvery = true;
                    for (Group j : d) {
                        if (Arrays.stream(j.getData()).filter(el -> el == elem).count() == 0) {
                            containInEvery = false;
                        }
                    }
                    if (containInEvery) {
                        l.add(elem);
                    }
                }
            }
        }
        return l;
    }
}
