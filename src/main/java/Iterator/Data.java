package Iterator;

import java.util.Iterator;

    public class Data implements Iterable<Group> {
        private Group[] arr;
        private String id;

        public Data(String id,Group... arr) {
            this.arr = arr;
            this.id = id;
        }

        public Group[] getArr() {
            return arr;
        }

        public void setArr(Group[] arr) {
            this.arr = arr;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public Iterator<Group> iterator() {
            return new Iterator<Group>() {
                private int index = - 1;
                @Override
                public boolean hasNext() {
                    return index != arr.length - 1;
                }

                @Override
                public Group next() {
                    if(!hasNext()){
                        throw new IndexOutOfBoundsException("End of array");
                    }
                    index++;
                    return arr[index];
                }
            };
        }
    }
