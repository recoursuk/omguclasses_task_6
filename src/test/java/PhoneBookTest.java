import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PhoneBookTest {

    @Test
    public void testDeleteNumber() throws NullPointerException{
        PhoneBook phoneBook = new PhoneBook();
        Human human = new Human("Mark", "Manson", "", 34);

        phoneBook.addNumber(human,"+79131469965");
        phoneBook.addNumber(human,"+79136789574");
        phoneBook.removeNumber(human, "+79131469965");

        List<String> rightAnswer = new LinkedList<>();
        rightAnswer.add("+79136789574");

        Assert.assertEquals(rightAnswer, phoneBook.getNumberList(human));
    }

    @Test
    public void testAddNumber() throws NullPointerException{
        PhoneBook phoneBook = new PhoneBook();
        Human human = new Human("Mark", "Manson", "", 34);
        phoneBook.addNumber(human,"1");
        phoneBook.addNumber(human,"2");

        List<String> rightAnswer = new LinkedList<>();
        rightAnswer.add("1");
        rightAnswer.add("2");

        Assert.assertEquals(rightAnswer, phoneBook.getNumberList(human));
    }

    @Test
    public void testFindHumanByNumber() throws NullPointerException{
        PhoneBook phoneBook = new PhoneBook();
        Human human1 = new Human("Mark", "Manson", "", 34);
        Human human2 = new Human("Lisa", "Huston", "", 27);
        phoneBook.addNumber(human1,"+79131469965");
        phoneBook.addNumber(human1,"+79136789574");
        phoneBook.addNumber(human2,"+75847839392");

        Assert.assertEquals(human1, phoneBook.find("+79136789574"));
        Assert.assertEquals(human2, phoneBook.find("+75847839392"));
    }

    @Test
    public void testGetRelevantMap() throws NullPointerException{
        PhoneBook phoneBook = new PhoneBook();
        Human human1 = new Human("Mark", "Handsome", "", 34);
        Human human2 = new Human("Lisa", "Huston", "", 27);
        Human human3 = new Human("Mary", "Huston", "", 22);

        phoneBook.addNumber(human1,"1");
        phoneBook.addNumber(human2,"2");
        phoneBook.addNumber(human3,"3");

        Map<Human, List<String>> rightAnswer = new HashMap<>();
        rightAnswer.put(human2, new LinkedList<String>());
        rightAnswer.put(human3, new LinkedList<String>());
        rightAnswer.get(human2).add("2");
        rightAnswer.get(human3).add("3");

        Assert.assertEquals(phoneBook.getPhoneBook(), phoneBook.getMapStartWith("H"));
        Assert.assertEquals(rightAnswer, phoneBook.getMapStartWith("Hu"));
    }
}
