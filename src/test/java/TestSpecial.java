
import Iterator.Data;
import Iterator.DataDemo;
import Iterator.Group;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestSpecial {

    @Test
    public void testGetAll(){
        Group group1 = new Group(1, 1, 2, 3, 4);
        Group group2 = new Group(2, 5, 1, 2, 6, 7);
        Group group3 = new Group(3, 8, 1, 2, 9, 10, 11, 12, 13, 14, 15);
        Data data = new Data("Groups 1-3", group1, group2, group3);

        List<Integer> expected = new ArrayList<>();

        for(int i = 1; i <= 2; i++){
            expected.add(i);
        }

        Assert.assertEquals(expected, DataDemo.getAll(data));
    }
}
